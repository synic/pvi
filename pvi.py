#!/usr/bin/env python

# Copyright (C) 2006 Adam Olsen
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 1, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

import sys, os, re, tempfile, getpass, sha
import time
from Crypto.Cipher import Blowfish
import subprocess

def scanfile(file):
    data = open(file).read()
    reg = re.compile(r'"""## ENC START ' + r'##\n*(.*)\n*## ENC END ##"""', 
        re.DOTALL|re.MULTILINE)
    m = reg.search(data)
    if not m or not m.group(1): return ""
    else: return m.group(1)

def encrypt(data, key):
    outstring = ""
    data = "_encrypted_" + data
    padding = 8 - len(data) % 8

    iv = sha.new(str(time.time())).hexdigest()[:8]

    enc = Blowfish.new(key, Blowfish.MODE_CBC, iv)
    data += "X" * padding
    data = enc.encrypt(data)

    outstring = "%s%d%s" % (iv, padding, data.encode('base64'))
    return outstring

def decrypt(data, key):
    iv = data[:8]
    padding = int(data[8:9])

    data = data[9:].decode('base64')

    enc = Blowfish.new(key, Blowfish.MODE_CBC, iv)
    data = enc.decrypt(data)

    data = data[:-padding]
    if not data.startswith('_encrypted_'):
        raise Exception("Decryption failed!")
    else:
        data = data.replace('_encrypted_', '')
    return data

(handle, tmp) = tempfile.mkstemp()
filedata = scanfile(__file__)

if not filedata:
    print "This is a new file."

    ok = False
    while not ok:
        password = getpass.getpass("Enter the new password: ")
        verify = getpass.getpass("Verify the password: ")
        if password != verify:
            print "Password and verification did not match."
        else:
            ok = True
else:
    password = getpass.getpass("Enter the password: ")
    try:
        data = decrypt(filedata, password)
    except:
        print "Decryption failed."
        sys.exit(1)

    h = open(tmp, 'w')
    h.write(data)
    h.close()

editor = os.getenv('EDITOR')
if not editor:
    editor = 'vi'
retval = subprocess.call([editor, tmp])
if retval != 0:
    print "Error running editor"
    os.unlink(tmp)
    sys.exit(1)

data = open(tmp).read()
os.unlink(tmp)

clearfile = False
if data.strip() == '':
    clearfile = True

## find out if they want to change the password
if data.find('__changepass__') > -1:
    data = data.replace('__changepass__', '')
    ok = False
    while not ok:
        password = getpass.getpass("Enter the new password: ")
        verify = getpass.getpass("Verify the password: ")
        if password != verify:
            print "Password and verification did not match."
        else:
            ok = True

data = encrypt(data, password)

lines = open(__file__).readlines()

h = open(__file__, 'w')
found = False
for line in lines:
    if line.startswith('"""## ENC START'):
        found = True
        h.write(line)
        break
    h.write(line)

if not found:
    h.write('"""## ENC' + ' START ##\n')

if clearfile: 
    data = ''
    print "Resetting file..."
h.write('\n%s\n## ENC END ##"""' % data)
h.close()

print "Done.\n"
"""## ENC START ##
## ENC END ##"""
